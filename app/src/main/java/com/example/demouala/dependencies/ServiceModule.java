package com.example.demouala.dependencies;

import android.content.Context;

import com.example.demouala.service.RetrofitInstance;
import com.example.demouala.service.libros.BooksService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ServiceModule {

    @Singleton
    @Provides
    public RetrofitInstance providesRetrofit(Context context) {
        return new RetrofitInstance(context);
    }

    @Singleton
    @Provides
    public BooksService providesNotesService(){return new BooksService();}

}
