package com.example.demouala.ui.libros;

import com.example.demouala.model.Libro;
import com.example.demouala.service.libros.BooksService;
import com.example.demouala.service.libros.IGenericService;
import com.example.demouala.ui.MainActivity;

import java.util.ArrayList;

import javax.inject.Inject;

public class BooksInteractor implements IBooksInteractor {

    private ArrayList<Libro> libros;

    @Inject
    BooksService booksService;

    public BooksInteractor() {
        MainActivity.getContactsComponents().inject(this);
    }

    @Override
    public void getLibros(IGetNotesListener listener) {
        booksService.doGetRequest(new IGenericService.IResponseListener<Libro>() {
            @Override
            public void onResponseSuccess(ArrayList<Libro> objects) {
                libros = objects;
                listener.onLoaded(libros);
            }

            @Override
            public void onResponseError(int errorCode, String message) {
                listener.showError(errorCode, message);
            }
        });
    }

    @Override
    public void onFinalize() {
        //TODO avoid memory leaks
    }
}
