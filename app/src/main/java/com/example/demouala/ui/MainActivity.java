package com.example.demouala.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

import com.example.demouala.dependencies.ApplicationComponents;
import com.example.demouala.dependencies.ApplicationModule;
import com.example.demouala.dependencies.DaggerApplicationComponents;

public class MainActivity extends Application {

    public static ApplicationComponents components;
    static MainActivity instance;

    @Override
    public void onCreate() {
        super.onCreate();
        initializateMShopitComponent();
        instance = this;

        getContactsComponents().inject(this);


    }

    public static ApplicationComponents getContactsComponents() {
        return components;
    }

    private void initializateMShopitComponent() {
        components = DaggerApplicationComponents.builder().applicationModule(new ApplicationModule(this)).build();

    }

    public static Context getContext(){ return instance;}

}
