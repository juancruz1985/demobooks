package com.example.demouala.dependencies;

import com.example.demouala.service.libros.BooksService;
import com.example.demouala.ui.MainActivity;
import com.example.demouala.ui.libros.BooksInteractor;
import com.example.demouala.ui.libros.BooksPresenter;

import javax.inject.Singleton;
import dagger.Component;


@Singleton
@Component(modules = {ApplicationModule.class,
                      BusinessModule.class,
                      ServiceModule.class})

public interface ApplicationComponents {
    void inject(MainActivity application);
    void inject(BooksService booksService);
    void inject(BooksInteractor booksInteractor);
    void inject(BooksPresenter booksPresenter);
}
