package com.example.demouala.ui.libros;

import com.example.demouala.model.Libro;
import com.example.demouala.ui.MainActivity;

import java.util.ArrayList;

import javax.inject.Inject;

public class BooksPresenter implements IBooksInteractor.IGetNotesListener {

    private IBooksView view;

    @Inject
    BooksInteractor interactor;

    public BooksPresenter(IBooksView view) {
        this.view = view;
        MainActivity.getContactsComponents().inject(this);
    }

    public void loadBooks(){
        interactor.getLibros(this);
    }

    @Override
    public void onLoaded(ArrayList<Libro> libros) {
        view.dataLoaded(libros);
    }

    @Override
    public void showError(int errorCode, String Message) {
        this.view.showError(errorCode, Message);
    }

    @Override
    public void onFinalize() {
        interactor.onFinalize();
    }
}
