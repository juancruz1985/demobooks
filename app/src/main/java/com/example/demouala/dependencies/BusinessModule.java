package com.example.demouala.dependencies;

import com.example.demouala.ui.libros.BooksInteractor;
import com.example.demouala.ui.libros.IBooksInteractor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class BusinessModule {

    @Singleton
    @Provides
    public BooksInteractor providesINotesInteractor(){return new BooksInteractor();}
}
