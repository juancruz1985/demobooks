package com.example.demouala.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.demouala.R;
import com.example.demouala.model.Libro;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class BooksViewAdapter extends RecyclerView.Adapter<BooksViewAdapter.ViewHolder> {

    private ArrayList<Libro> libros;
    private Context context;

    public BooksViewAdapter(ArrayList<Libro> libros, Context context) {
        this.context = context;
        this.libros = libros;
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView iv_foto;
        private TextView tv_nombre;
        private TextView tv_autor;
        private TextView tv_popularidad;
        private TextView tv_estado;


        public ViewHolder(View itemView) {
            super(itemView);
            iv_foto = itemView.findViewById(R.id.iv_foto);
            tv_nombre = itemView.findViewById(R.id.tv_nombre);
            tv_autor = itemView.findViewById(R.id.tv_autor);
            tv_popularidad = itemView.findViewById(R.id.tv_popularidad);
            tv_estado = itemView.findViewById(R.id.tv_estado);
        }

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //Create a new item view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lista_item, parent, false);
        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull BooksViewAdapter.ViewHolder holder, int position) {
        Libro libro = libros.get(position);
        if (!libro.getImagen().equals("")) {
            Picasso.with(this.context).load(libro.getImagen()).into(holder.iv_foto);
        }
        holder.tv_popularidad.setText(libro.getPopularidad().toString());
        holder.tv_autor.setText(libro.getAutor());
        holder.tv_nombre.setText(libro.getNombre());
        if (libro.getDisponibilidad()) {
            holder.tv_estado.setText("Disponible");
        }else{
            holder.tv_estado.setText("No Disponible");
        }
    }

    @Override
    public int getItemCount() {
        return libros.size();
    }
}
